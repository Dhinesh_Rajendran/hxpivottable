import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HxLayoutComponent } from './hx-pivot-layout.component';

describe('HxLayoutComponent', () => {
  let component: HxLayoutComponent;
  let fixture: ComponentFixture<HxLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HxLayoutComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HxLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
