/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SubSink } from 'subsink'
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep, min, max, constant } from 'lodash';
import { HxPivotTableService } from '../../shared/services/hx-pivot-table.service';
import {
  HxPivotColumns, HxPivotSortEvent, HxPivotStructure,
  HxPivotFilterEvent, HxPivotSelectedFilter, HxPivotHeatMap
} from '../../shared/interfaces/hx-pivot-table';
import { HxPivotSort } from '../../shared/directives/hx-pivot-sort/hx-pivot-sort.directive';
import { HxPivotSortAs } from '../../shared/enum/hx-pivot-table.enum';
import { HxPivotFilter } from '../../shared/directives/hx-pivot-filter/hx-pivot-filter.directive';
import { HxPivotFilterModalComponent } from '../hx-pivot-filter-modal/hx-pivot-filter-modal.component';
import { HxPivotMetricsModalComponent } from '../hx-pivot-metrics-modal/hx-pivot-metrics-modal.component';
import { HxPivotHeatmapComponent } from '../hx-pivot-heatmap/hx-pivot-heatmap.component';
import { HxPivotConditionalFormattingComponent } from
  '../hx-pivot-conditional-formatting/hx-pivot-conditional-formatting.component';

@Component({
  selector: 'app-hx-pivot-layout',
  templateUrl: './hx-pivot-layout.component.html',
  styleUrls: ['./hx-pivot-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HxPivotLayoutComponent implements AfterViewInit, OnDestroy {
  @Input() height = 450;
  @Input() width: string | number = "100%";

  pivotDataSource$ = new BehaviorSubject<any>(null);
  headers$ = new BehaviorSubject<HxPivotColumns[]>([]);
  dataSource$ = new BehaviorSubject<any[]>([]);
  displayColumns$ = new BehaviorSubject<Array<string>>([]);
  rows$ = new BehaviorSubject<Array<string>>([]);
  scrollLeft$ = new BehaviorSubject<string>('0');
  hxLoader$ = new BehaviorSubject<boolean>(false);
  heatmap$ = new BehaviorSubject<HxPivotHeatMap[]>([]);
  conditions$ = new BehaviorSubject<any>([]);

  @ViewChild(CdkVirtualScrollViewport, { static: true }) viewPort: CdkVirtualScrollViewport;
  @ViewChild(HxPivotSort) sort: HxPivotSort;
  @ViewChild(HxPivotFilter) filter: HxPivotFilter;

  private subs = new SubSink();
  private modalRef: NgbModalRef;

  constructor(private cdr: ChangeDetectorRef,
    private zone: NgZone,
    private modal: NgbModal,
    private hxPivotTableService: HxPivotTableService) { }

  /**
   * Listening events here
   * while sort, filter and scroll changes, this will be executed
   * Dhinesh
   * May 6th, 2021
   */
  ngAfterViewInit(): void {
    this.subs.add(this.sort.sortChange.subscribe((sort: HxPivotSortEvent) =>
      this.sorting(sort))
    );
    this.subs.add(this.filter.filterChange.subscribe((filter: HxPivotFilterEvent) =>
      this.openFilter(filter))
    );
    this.subs.add(this.viewPort.elementScrolled().subscribe((res: any) =>
      this.updateLeftPosition(res.target.scrollLeft))
    );
    // this.viewPort.renderedRangeStream.subscribe(({ start, end }) => console.log(start, end));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  setReport(pivotStructure: HxPivotStructure): void {
    this.hxLoader$.next(true);
    this.clear();
    setTimeout(() => {
      this.pivotDataSource$.next(cloneDeep(pivotStructure));
      this.headers$.next(this.hxPivotTableService.getHeaders(pivotStructure.slice, pivotStructure.dataSource.mapping));
      this.heatmap$.next(
        [...this.hxPivotTableService.getSelectedHeatMapMeasures(
          pivotStructure.heatmap,
          pivotStructure.dataSource.data,
          this.headers$.value)
        ]
      );
      this.conditions$.next(pivotStructure.conditions);
      this.displayColumns$.next([... this.headers$.value.map(({ uniqueName }) => uniqueName)]);
      this.rows$.next([...pivotStructure.slice.rows.map(({ uniqueName }) => uniqueName)]);
      this.dataSource$.next([...pivotStructure.dataSource.data]);
      this.hxLoader$.next(false);
      this.cdr.detectChanges();
    }, 500);
  }

  trackByRow(index: number): number {
    return index;
  }

  trackByKey(_: number, uniqueName: string): string {
    return uniqueName;
  }

  clear(): void {
    this.headers$.next([]);
    this.rows$.next([]);
    this.dataSource$.next([]);
    this.displayColumns$.next([]);
    this.scrollLeft$.next('0');
  }

  refresh(): void {
    this.dataSource$.next([...this.dataSource$.value]);
  }

  openConditionalFormatting(): void {
    const modalRef = this.modal.open(HxPivotConditionalFormattingComponent, { size: 'lg' });
    modalRef.componentInstance.conditionalFormatting = [];
    this.subs.add(
      modalRef.componentInstance.applyButtonClicked.subscribe((measures: HxPivotHeatMap[]) => {
        this.heatmap$.next(measures);
        this.refresh();
        modalRef.close();
      }));
    this.subs.add(modalRef.componentInstance.cancelButtonClicked.subscribe(() => modalRef.close()));
  }
  /**
   * Opening custom heatmap modal
   * Dhinesh
   */
  openHeatMap(): void {
    const modalRef = this.modal.open(HxPivotHeatmapComponent, { size: 'lg' });
    modalRef.componentInstance.heatMapConfig = cloneDeep({
      allMeasures: this.hxPivotTableService.getMeasures(this.headers$.value)
        .map(({ uniqueName, caption }) => {
          return {
            caption,
            uniqueName,
          }
        }),
      selectedMeasures: this.heatmap$.value,
    },
    );
    this.subs.add(
      modalRef.componentInstance.applyButtonClicked.subscribe((measures: HxPivotHeatMap[]) => {
        this.heatmap$.next(measures);
        this.refresh();
        modalRef.close();
      }));
    this.subs.add(modalRef.componentInstance.cancelButtonClicked.subscribe(() => modalRef.close()));
  }


  /**
   * Update the options selected by the user
   * Delete the particular header
   * Insert the new header with same properties with updated options to the same deleted index.
   * So it should be passed with new reference.
   * Call the function to filter the dataSource based on selected options.
   *
   * Dhinesh
   * May 5th, 2021
   * @param filter
   */
  private handleAppliedFilter(f: HxPivotSelectedFilter): void {
    const headers = [...this.headers$.value];
    let header: HxPivotColumns;
    const index: number = headers.findIndex(({ uniqueName }) => uniqueName === f.uniqueName);
    if (index > -1) {
      header = cloneDeep(headers[index]);
      if (f.filter) header.filter = { ...f.filter };
      else if (f.metrics?.operator) header.filter.metrics = { ...f.metrics }
      headers.splice(index, 1);
      headers.splice(index, 0, header);
    }
    this.headers$.next([...headers]);
    this.updateDataSource();
  }



  private closeModal(): void {
    this.modalRef.close();
  }


  /**
  * Filter the dataSource based on the selected options by the user.
  * Dhinesh
  * 5th may 2021
  */
  private updateDataSource(): void {
    const t0 = performance.now();
    const dataSource = this.hxPivotTableService.filterDataSource(
      this.pivotDataSource$.value.dataSource.data,
      this.headers$.value);
    const t1 = performance.now();
    console.log(`${t1 - t0} milliseconds.`);
    //  if (dataSource.length === 0) dataSource = this.addCellsWhileNoDataSourceExist();
    this.dataSource$.next([...dataSource]);
  }

  private addCellsWhileNoDataSourceExist(): any[] {
    return this.headers$.value.map(({ uniqueName }) => {
      return {
        [uniqueName]: null
      }
    })
  }

  private updateLeftPosition(leftPosition: number): void {
    this.zone.run(() => {
      this.scrollLeft$.next(`-${leftPosition}`);
      this.cdr.detectChanges();
    });
  }


  /**
   * This will be executed while click the sort icon (ASC and DESC)
   * Dhinesh
   * @param sort
   * @private
   */
  private sorting(sort: HxPivotSortEvent): void {
    const t0 = performance.now();
    if (sort.direction === HxPivotSortAs.ASC)
      this.dataSource$.next(
        this.hxPivotTableService.sortAsc({ uniqueName: sort.active, type: sort.type }, this.dataSource$.value)
      );
    else
      this.dataSource$.next(
        this.hxPivotTableService.sortDesc({ uniqueName: sort.active, type: sort.type }, this.dataSource$.value)
      );

    const t1 = performance.now();
    console.log(`${t1 - t0} milliseconds.`);

    /**
     * Using map function to get the new reference of the headers
     * Dhinesh
     * May 12th, 2021
     */
    const headers = this.headers$.value.map((f: HxPivotColumns) => {
      return {
        ...f,
        sort: sort.active === f.uniqueName ? sort.direction : 'reset',
      }
    });
    this.headers$.next(headers);
  }

  /**
   * Open filter modal for the filter options.
   * @param filterEvent
   * @private
   */
  private openFilter(filterEvent: HxPivotFilterEvent): void {
    this.modalRef = this.modal.open(
      filterEvent.headerType === 'row' ? HxPivotFilterModalComponent : HxPivotMetricsModalComponent);
    const selectedFilter: HxPivotSelectedFilter = {};
    if (filterEvent.headerType === 'column')
      Object.assign(selectedFilter, {
        operator: filterEvent.metrics.operator,
        value: filterEvent.metrics.value,
        header: filterEvent.header,
      })
    else
      Object.assign(selectedFilter, {
        header: filterEvent.header,
        filter: filterEvent.filter,
        options: [...new Set(this.pivotDataSource$.value.dataSource.data.map((d: any) =>
          d[filterEvent.header.uniqueName])
        )]
      });
    this.modalRef.componentInstance.selectedFilter = { ...selectedFilter };
    this.subs.add(this.modalRef.componentInstance.applyButtonClicked.subscribe((f: HxPivotSelectedFilter) => {
      this.closeModal();
      this.handleAppliedFilter(f)
    }));
    this.subs.add(this.modalRef.componentInstance.cancelButtonClicked.subscribe(() => this.closeModal()));
  }
}




