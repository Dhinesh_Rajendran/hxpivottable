import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { HxPivotColumns } from '../../shared/interfaces/hx-pivot-table';
@Component({
  selector: 'hx-pivot-header',
  templateUrl: './hx-pivot-header.component.html',
  styleUrls: ['./hx-pivot-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HxPivotHeaderComponent {
  @Input() headers: HxPivotColumns[] = [];


  trackByHeader(_: number, header: HxPivotColumns): string {
    return header.uniqueName;
  }

  resize(event: any): void {
    console.log(event, 'event');
  }
}

