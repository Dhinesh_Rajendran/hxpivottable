import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HxPivotHeaderComponent } from './hx-pivot-header.component';

describe('HxPivotHeaderComponent', () => {
  let component: HxPivotHeaderComponent;
  let fixture: ComponentFixture<HxPivotHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HxPivotHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HxPivotHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
