import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HxPivotVirtualScrollComponent } from './hx-pivot-virtual-scroll.component';

describe('HxPivotVirtualScrollComponent', () => {
  let component: HxPivotVirtualScrollComponent;
  let fixture: ComponentFixture<HxPivotVirtualScrollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HxPivotVirtualScrollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HxPivotVirtualScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
