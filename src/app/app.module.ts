import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HxPivotTableModule} from './hx-pivot-table/hx-pivot-table.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HxPivotTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
